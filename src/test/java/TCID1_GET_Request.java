import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TCID1_GET_Request {
	
	@Test
	void getWeatherDetails()
	{
		
		//Specify base URI
		RestAssured.baseURI="https://reqres.in/";
		
		//Request object
		RequestSpecification httpRequest = RestAssured.given();
		
		//Response Object
		Response response =httpRequest.request(Method.GET,"api/users/2");
		
		//Print Response in console 
		String responseBody=response.getBody().asString();
		
		System.out.println("Response Body is : "+responseBody);
		
		//Status Code Validation
		int statusCode=response.getStatusCode();
		System.out.println("Status Code is :"+statusCode);
		Assert.assertEquals(statusCode, 200);
		
		//Status Line Verification
		String statusLine=response.getStatusLine();
		System.out.println("Status Line :"+statusLine);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
		
		
		
	}
}
