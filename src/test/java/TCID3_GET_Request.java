	
	import org.testng.Assert;
	import org.testng.annotations.Test;

	import io.restassured.RestAssured;
	import io.restassured.http.Method;
	import io.restassured.response.Response;
	import io.restassured.specification.RequestSpecification;

	public class TCID3_GET_Request {
		
		@Test
		void googleMapTestDTO()
		{
			
			//Specify base URI
			RestAssured.baseURI="https://maps.googleapis.com";
			
			//Request object
			RequestSpecification httpRequest = RestAssured.given();
			
			//Response Object
			Response response =httpRequest.request(Method.GET,"/maps/api/place/nearbysearch/xml?location=-33.8670522,151.1957362&radius=1500&type=supermarket&key=AIzaSyBjGCE3VpLU4lgTqSTDmHmJ2HoELb4Jy1s");
			
			//Print Response in console 
			String responseBody=response.getBody().asString();
			System.out.println("Response Body is : "+responseBody);
			
			//Capture details of Header from response
			
			String contentType=response.header("Content-Type");
			System.out.println("Content Type is : "+contentType);
			Assert.assertEquals(contentType, "application/xml; charset=UTF-8");
			
			String cacheControl=response.header("Cache-Control");
			System.out.println("Cache-Control Type is : "+cacheControl);
			Assert.assertEquals(cacheControl, "no-cache, must-revalidate");
			
			
			
			
			
		}
	}



