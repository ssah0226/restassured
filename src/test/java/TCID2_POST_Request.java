import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TCID2_POST_Request {
	
	@Test
	void registerCustomerAPI() {
		//Specify base URI
				RestAssured.baseURI="https://reqres.in/";
				
				//Request object
				RequestSpecification httpRequest = RestAssured.given();
				
				//Request payload sending along with post request
				JSONObject requestParams = new JSONObject();
				
				requestParams.put("name", "Sunil");
				requestParams.put("job", "HR");
				
				httpRequest.header("Content-Type","application/json");
				
				httpRequest.body(requestParams.toJSONString());
				
				Response response =httpRequest.request(Method.POST,"api/users");
				
				//Status Code Validation
				int statusCode=response.getStatusCode();
				System.out.println("Status Code is :"+statusCode);
				Assert.assertEquals(statusCode, 201);
				
				//successCode validation
//				String successCode=response.jsonPath().get("SucessCode");
//				System.out.println("Success Code :"+successCode);
//				Assert.assertEquals(successCode, "OPERATION_SUCCESS");
				
				
//				
				
	}

}
